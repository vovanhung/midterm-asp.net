﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MID_EXAM_VOVANHUNG_2321122017.App_Code;

namespace MID_EXAM_VOVANHUNG_2321122017
{
    public partial class MasterPage : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String path = Server.MapPath("App_Data\\dbTHIETBIMAYTINHCU.mdf");
            XuLyDuLieu xulydulieu = new XuLyDuLieu(path);
            String SQL = " select * from NHOMTHIETBI";
            this.Repeater1.DataSource = xulydulieu.layBang(SQL);
            this.Repeater1.DataBind();
        }
    }
}